#!/usr/bin/env python3
import paramiko
import argparse
import ipaddress
import ping3

parser = argparse.ArgumentParser(prog='ssh_net_run', description='Simple script to try to connect to every ip in a network and execute a shell command over ssh ')
parser.add_argument('-ip',dest='ip', help='Specify the start IPv4 address in CIDR format. Example: -ip 192.168.0.2/24', required=True)
parser.add_argument('-p', '--port',dest='port', default=22, type=int)
parser.add_argument('-u', '--username',dest='username', help='The username to log into the ssh session', required=True)
parser.add_argument('-pw', '--password', dest='password', help='The password to log into the ssh session', required=True)
parser.add_argument('-r', '--run',dest='run', help='The command to run after a successful login. Example: --run \'ls -la\'')
parser.add_argument('--ping-timeout', dest='ping_timeout', default=0.5, help='Ping timeout in seconds', type=float)
parser.add_argument('--ssh-timeout', dest='ssh_timeout', default=1, help='Timeout in seconds for the TCP connect', type=float)

args=parser.parse_args()


count_checked_hosts = 0
count_available_hosts = 0
count_successful_hosts = 0
count_failed_auth_hosts = 0
count_failed_connect_hosts = 0
count_failed_exec_hosts = 0

def tryToConnectAndRunCommand(host, port, username, password, run):
    print('Pinging '+host+'...')
    ping_is_available = ping3.ping(host, size=4, timeout=args.ping_timeout)
    if(not ping_is_available):
        return

    global count_available_hosts
    count_available_hosts += 1


    print('Trying to connect to '+host+'...')
    client=paramiko.client.SSHClient()
    client.set_missing_host_key_policy(policy=paramiko.AutoAddPolicy)
    try:
        client.connect(host,port, username, password, timeout=args.ssh_timeout)
    except paramiko.AuthenticationException:
        client.close()
        global count_failed_auth_hosts
        count_failed_auth_hosts += 1
        print(host+' - Failed auth')
        return
    except Exception as e:
        client.close()
        global count_failed_connect_hosts
        count_failed_connect_hosts += 1
        print(host+' - Failed connect')
        return
    try:
        client.exec_command(args.run)
    except paramiko.SSHException:
        client.close()
        global count_failed_exec_hosts
        count_failed_exec_hosts += 1
        print(host+' - Failed executing command')

    client.close()
    global count_successful_hosts
    count_successful_hosts += 1
    print(host+' - Success')



interface=ipaddress.IPv4Interface(args.ip)
start_ip=interface.ip
network=interface.network

print()
print('Starting from address '+start_ip.exploded+' we will run "'+args.run+'" on any machine on the network '+network.network_address.exploded )
print('Username: '+args.username)
print('Port: '+str(args.port))
print()



begin_connecting = False
for host in network.hosts():
    if(host.packed == start_ip.packed):
        begin_connecting = True

    if(begin_connecting and not host.is_reserved):
        count_checked_hosts += 1
        tryToConnectAndRunCommand(host.exploded, args.port, args.username, args.password, args.run)

print('Checked: '+str(count_checked_hosts))
print('Available: '+str(count_available_hosts))
print('Successful: '+str(count_successful_hosts))
print('Failed connection: '+str(count_failed_connect_hosts))
print('Failed authentication: '+str(count_failed_auth_hosts))
print('Failed command execution: '+str(count_failed_exec_hosts))



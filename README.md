# ssh_net_run

Tool to scan a network for ssh servers, login and run a command

## How to use

1. git clone this repository
2. cd into the folder
3. optionally create and activate a virtual enviroment with "python -m venv venv && source venv/bin/activate"
4. install dependencies from requirements.txt with "pip install -r requirements.txt"
5. run the help command of the script with " ./ssh_net_run.py -h "
